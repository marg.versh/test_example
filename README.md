## Running tests
1) Mark required tests with an appropriate decorator
(see pytest.ini file). You can provide any order using @pytest.mark.run;
2) Create a command according to the fixtures and pytest options you need.

Example:

pytest -m smoke --server test-1

where:

"-m smoke" - runs tests with the "smoke" decorator. You can use
"--browser firefox" - runs tests on the particular browser (Chrome by default, Firefox
is availible too);
"--server test-1" - runs tests on the particular server.

Use pytest -k [name of a test] to run a particular test only.

## Poetry env installation
https://python-poetry.org/