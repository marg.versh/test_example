from base.http.auth import BearerAuthClient
from base.http.base_http import BaseHttp


class ApiClient(BaseHttp):
    def __init__(self, server):
        super().__init__(server)
        self.auth = BearerAuthClient(server)

    def _make_request(self, method, endpoint, **kwargs):
        self.auth.inject_token()
        kwargs.setdefault('auth', self.auth.auth_method(self.auth.auth_token))
        return super()._make_request(method, endpoint, **kwargs)
