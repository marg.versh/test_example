from urllib.parse import urljoin
from requests.auth import AuthBase
from base.http.base_http import BaseHttp

import settings

class BearerTokenAuth(AuthBase):
    """AuthBase extension"""
    def __init__(self, token):
        self.token = token

    def __call__(self, request):
        request.headers['authorization'] = f'Bearer {self.token}'
        return request


class BearerAuthClient(BaseHttp):
    """Makes Bearer authorization"""
    def __init__(self, server):
        super().__init__(server)
        self.server = server
        self.auth_method = BearerTokenAuth
        self.auth_token = None
        self.refresh_token = None

    def inject_token(self):
        self.auth_token = settings.config.get("BEARER_TOKEN")
        self.refresh_token = settings.config.get("REFRESH_TOKEN")

    def _refresh_token(self):
        # Has to get modified later
        refresh_url = urljoin(self.server, settings.REFRESH_ENDPOINT.lstrip('/'))
        response = self.post(refresh_url, json={
            'refresh': self.refresh_token
        })
        self.auth_token = response['access']
        self.refresh_token = response['refresh']
 