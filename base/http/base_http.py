"""Makes base http requests"""
from urllib.parse import urljoin
from requests import request


class BaseHttp:
    def __init__(self, base_url):
        """Base requests"""
        self.base_url = base_url
        if not base_url.endswith('/'):
            self.base_url += '/'

    def _make_request(self, method, endpoint, **kwargs):
        """Makes a request"""
        endpoint_url = urljoin(self.base_url, endpoint.lstrip('/'))
        req = request(method, endpoint_url, **kwargs)
        req.raise_for_status()
        if req.text:
            return req.json()
        return req.status_code
