"""Defines an env"""
import logging
from urllib.parse import urljoin
from selenium import webdriver
import chromedriver_autoinstaller
import geckodriver_autoinstaller
import settings
from settings import config


class WebFactory:
    """Defines an environment to run tests"""
    logger = logging.getLogger(__name__)
    servers = {
        'test-1': config.get('TEST_STAND_1'),
        'test-2': config.get('TEST_STAND_2'),
    }

    def __init__(self, browser, server):
        self.browser = browser
        self.server = server

    def get_driver(self):
        """Gets a driver to run"""
        if self.browser == "firefox":
            geckodriver_autoinstaller.install()
            options = webdriver.FirefoxOptions()
            prefs = {"profile.default_content_setting_values.notifications" : 2}
            options.set_preference('dom.webnotifications.enabled', False)
            driver = webdriver.Firefox(options=options)
        else:
            chromedriver_autoinstaller.install()
            options = webdriver.ChromeOptions()
            prefs = {"profile.default_content_setting_values.notifications" : 2}
            options.add_experimental_option("prefs", prefs)
            driver = webdriver.Chrome(options=options)

        driver.implicitly_wait(settings.WAIT_AVERAGE)
        driver.get(urljoin(self.server, settings.MAIN_PAGE))
        driver.maximize_window()

        return driver

    @property
    def get_server(self):
        """Gets a server"""
        return self.servers[self.server]
