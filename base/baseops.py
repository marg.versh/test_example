"""General test methods"""
import logging
import time
import settings

from base.selenium_custom_driver import SeleniumCustomDriver


class BaseOps(SeleniumCustomDriver):
    """General WEB verifications"""
    logger = logging.getLogger(__name__)

    def is_element_displayed(self, locator, locator_type=settings.XPATH):
        """Veryfing if an element is displayed"""
        result = False
        element = self.find_an_element(locator, locator_type)
        time.sleep(settings.WAIT_SECOND)
        # current frontend errors requires additional time to show up/
        # can't be processed via wait - don't have such parameters
        if element.is_displayed():
            self.logger.info("PASSED: ElEMENT DISPLAYED Verification")
            result = True
        else:
            self.logger.error("FAILED: ElEMENT DISPLAYED Verification")
        return result

    def is_element_enabled(self, locator, enable_condition, locator_type=settings.XPATH):
        """Veryfing if an element is en/disabled according to condition"""
        result = False
        element = self.find_an_element(locator, locator_type)
        if element.is_enabled() and enable_condition:
            self.logger.info("PASSED: ElEMENT ENABLED Verification")
            result = True
        if not element.is_enabled() and not enable_condition:
            self.logger.info("PASSED: ElEMENT DISABLED Verification")
            result = True
        else:
            self.logger.error("FAILED: UNEXPECTED CONDITION")
        return result

    def is_url_valid(self, expected_url, url_condition=settings.URL_TO_BE):
        """Verifying if a url is ok"""
        if url_condition == settings.URL_TO_BE:
            return expected_url == self.driver.current_url
        if url_condition == settings.URL_CONTAINS:
            return expected_url in self.driver.current_url

    def navigate_form(self, server_url, nav_url, back_condition=None):
        """Navigation within the app"""
        if back_condition is not None:
            self.wait_until_element(back_condition, settings.PR_EL_LOCATED,
            locator_type=settings.XPATH, click_condition=True)
        self.close_all_tabs()
        self.get_link(server_url, nav_url)


class APIBaseOps:
    """General API verifications"""
    logger = logging.getLogger(__name__)

    def is_response_valid(self, response, expected_response = None, new_keys = None):
        """Verifying if a response is ok"""
        result = False
        if new_keys is not None:
            for key, value in new_keys.items():
                if key not in response:
                    return False
                if not isinstance(response[key], value):
                    return False
                del response[key]
        if response in (expected_response, 200, 204):
            result = True
        return result
