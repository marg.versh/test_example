import logging
import logging.config
import os
import random
import secrets
import string
import time
from urllib.parse import urljoin
import selenium.common.exceptions as exc

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import settings


class SeleniumCustomDriver:
    """
    Custom webdriver with more constituent methods and logging info
    """
    logging.config.dictConfig(settings.LOGGING_CONFIG)
    logger = logging.getLogger(__name__)

    def __init__(self, driver):
        self.driver = driver

    def clear_field(self, locator, locator_type=settings.XPATH):
        """Clears a field"""
        element = self.find_an_element(locator, locator_type)
        element.clear()
        self.logger.info("Field cleared")

    def get_type(self, locator_type):
        """Returns an element's type"""
        try:
            by_type = {
            settings.ID: By.ID,
            settings.NAME: By.NAME,
            settings.XPATH: By.XPATH,
            settings.CSS: By.CSS_SELECTOR,
            settings.CLASS: By.CLASS_NAME,
            settings.LINK: By.LINK_TEXT
        }[locator_type]
        except KeyError:
            self.logger.exception("Couldn't find an element's type")
            raise
        return by_type

    def click_on_element(self, locator, locator_type=settings.XPATH):
        """Clicks on an element"""
        try:
            element = self.find_an_element(locator, locator_type)
            element.click()
        except (
            AttributeError, exc.InvalidElementStateException,
            exc.ElementClickInterceptedException):
            self.logger.exception('Couldnt click on the element: %s', locator)
            raise
        self.logger.info('Clicked on the element: %s', locator)

    def close_all_tabs(self):
        """Closes all tabs except 1 active"""
        current = self.driver.current_window_handle
        for handle in self.driver.window_handles:
            self.driver.switch_to.window(handle)
            if handle != current:
                self.driver.close()
        self.logger.info('All tabs closed')

    def create_mail(self):
        """Generates a test email"""
        random_mail = ''.join(random.choices(string.ascii_lowercase + string.digits, k=8))
        mailbox = f'{random_mail}@test.qa'
        self.logger.info("The mailbox created")
        return mailbox

    def create_password(self):
        """Generates a test password"""
        return secrets.token_urlsafe(10)

    def delete_cookies(self):
        self.driver.delete_all_cookies()
        self.logger.info('Cookies deleted')

    def find_an_element(self, locator, locator_type=settings.XPATH):
        """Finds an element"""
        try:
            getting_type = self.get_type(locator_type)
            element = self.driver.find_element(getting_type, locator)
        except exc.NoSuchElementException:
            self.logger.exception('Element %s not found', locator)
            raise
        self.logger.info('Element %s found', locator)
        return element

    def get_link(self, link, url=None):
        """Follows a link"""
        try:
            if url is not None:
                self.driver.get(urljoin(link, url))
            else:
                self.driver.get(link)
        except exc.WebDriverException:
            self.logger.exception('Link %s failed', link)
            raise
        self.logger.info('Link %s followed', urljoin(link, url))

    def refresh_page(self):
        """Refreshing a page"""
        self.driver.refresh()
        self.logger.info("The page refreshed successfully")

    def sending_keys(self, data, locator, locator_type=settings.XPATH):
        """Sending keys to a field"""
        try:
            element = self.find_an_element(locator, locator_type)
            element.send_keys(data)
        except exc.WebDriverException:
            self.logger.exception('Couldnt send data to the element %s', locator)
            raise
        self.logger.info('Sent data to the element %s', locator)

    def switching_handles(self, handle_number):
        """Switching windows"""
        handles = self.driver.window_handles
        self.driver.switch_to.window(handles[handle_number])
        check_handle = self.driver.current_window_handle
        if check_handle == handles[handle_number]:
            self.logger.info("The window switched to the handle")
        else:
            self.logger.error("The window not switched")

    def take_screenshot(self, test_name):
        """Takes a screenshot"""
        file_name = test_name + "." + str(round(time.time() * 1000)) + ".png"
        screenshot_directory = "../screenshots/"
        relative_file_name = screenshot_directory + file_name
        current_directory = os.path.dirname(__file__)
        destination_file = os.path.join(current_directory, relative_file_name)
        destination_directory = os.path.join(current_directory, screenshot_directory)

        if not os.path.exists(destination_directory):
            os.makedirs(destination_directory)
        self.driver.save_screenshot(destination_file)
        self.logger.info('Screenshot saved to %s', destination_file)

    def wait_until_element(self, locator, wait_condition,
                           locator_type=settings.XPATH, click_condition=False):
        """Waiting for a special condition"""
        wait = WebDriverWait(self.driver, 120, poll_frequency=0.5)
        wait_condition = {
            settings.EL_CLICKABLE: EC.element_to_be_clickable,
            settings.PR_EL_LOCATED: EC.presence_of_element_located,
            settings.TITLE_IS: EC.title_is,
            settings.FRAME_SWITCH: EC.frame_to_be_available_and_switch_to_it,
            settings.URL_TO_BE: EC.url_to_be,
            settings.URL_CONTAINS: EC.url_contains
        }[wait_condition]

        try:
            if locator_type is not None:
                conv_locator_type = self.get_type(locator_type)
                wait.until(wait_condition((conv_locator_type,locator)))
            else:
                wait.until(wait_condition((locator)))
        except (KeyError, exc.WebDriverException):
            self.logger.exception('Element %s not found after waiting', locator)
            raise

        if click_condition:
            self.click_on_element(locator, locator_type)

        self.logger.info('Element %s found after waiting', locator)
