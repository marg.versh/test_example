from time import sleep
import pytest
import settings

from pages.iframe_page import IframePage
from tests.datatest import data_iframe_form


@pytest.mark.smoke
@pytest.mark.iframe
class TestIframe:
    """Performs iframe tests"""

    @pytest.fixture(autouse=True)
    def page_set_up(self, driver_set_up, check_status_set_up):
        """Sets up a page object for tests to run"""
        self.user = IframePage(self.driver)
        self.user.delete_cookies()

    @pytest.mark.parametrize("fname, lname", data_iframe_form)
    def test_valid_filling_iframe_form(self, fname, lname):
        """Runs valid filling iframe form test"""
        self.user.navigate_form(self.server_url, settings.IFRAME_PAGE)
        self.user.fill_form(fname = fname, lname = lname)
        sleep(5) # just to have a look at the form
        result = self.user.is_element_displayed(settings.FILLED_IFRAME)
        self.status.mark_final("test_valid_filling_iframe_form", result)
        