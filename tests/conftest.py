"""
Fixtures
"""
import pytest

from base.baseops import APIBaseOps
from base.http.client import ApiClient
from base.webfactory import WebFactory
from utils.check_teststatus import CheckStatus


@pytest.fixture(scope="class")
def driver_set_up(request, browser, server_set_up):
    """Sets up basic driver settings"""
    print("Running driver_set_up setup")
    server = server_set_up
    web = WebFactory(browser, server)
    driver = web.get_driver()

    if request.cls is not None:
        request.cls.driver = driver

    yield driver
    driver.quit()
    print("Running driver_set_up teardown")

@pytest.fixture(scope="class")
def server_set_up(request, browser, server):
    """Sets up basic server settings"""
    print("Running server_set_up setup")
    web = WebFactory(browser, server)
    server_url = web.get_server

    if request.cls is not None:
        request.cls.server_url = server_url

    return server_url

@pytest.fixture(scope="class")
def check_status_set_up(request):
    """Sets up CheckStatus"""
    status = CheckStatus()

    if request.cls is not None:
        request.cls.status = status

    return status

@pytest.fixture(scope="class")
def api_client_set_up(request, server_set_up):
    """Sets up API client"""
    api_client = ApiClient(server_set_up)

    if request.cls is not None:
        request.cls.api_client = api_client

    return api_client

@pytest.fixture(scope="class")
def api_baseops_set_up(request):
    """Sets up API baseops"""
    api_baseops = APIBaseOps()

    if request.cls is not None:
        request.cls.api_baseops = api_baseops

    return api_baseops

def pytest_addoption(parser):
    """Parsing from the command line"""
    parser.addoption("--browser")
    parser.addoption("--server")

@pytest.fixture(scope="session")
def browser(request):
    """Parsing a browser"""
    return request.config.getoption("--browser")

@pytest.fixture(scope="session")
def server(request):
    """Parsing a server"""
    return request.config.getoption("--server")
