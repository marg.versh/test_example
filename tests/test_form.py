from time import sleep
from urllib.parse import urljoin
import pytest
import settings

from pages.form_page import FormPage
from tests.datatest import (data_form_valid)


@pytest.mark.smoke
@pytest.mark.form
class TestForm:
    """Performs form tests"""

    @pytest.fixture(autouse=True)
    def page_set_up(self, driver_set_up, check_status_set_up):
        """Sets up a page object for tests to run"""
        self.user = FormPage(self.driver)
        self.user.delete_cookies()

    @pytest.mark.parametrize(
            "fname, lname, ccode, pnumber, adline1, adline2, state, pcode, ctry, birth",
            data_form_valid)
    def test_valid_filling_form(
            self, fname, lname, ccode, pnumber, adline1, adline2,
            state, pcode, ctry, birth):
        """Runs valid filling form test"""
        self.user.navigate_form(self.server_url, settings.FORM_PAGE)
        self.user.fill_form(
            fname = fname, lname = lname, ccode = ccode,
            pnumber = pnumber, adline1 = adline1, adline2 = adline2,
            state = state, pcode = pcode, ctry = ctry, birth = birth)
        sleep(5) # just to have a look at the form
        result_element = self.user.is_element_displayed(settings.CHECK_EMAIL)
        self.status.set_result("test_valid_filling_form", result_element)
        result = self.user.is_url_valid(
            urljoin(self.server_url, settings.FORM_PAGE))
        self.status.mark_final("test_valid_filling_form", result)
        