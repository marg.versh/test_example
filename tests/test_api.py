import pytest
import settings


@pytest.mark.api
class TestApi:
    """Performs API tests"""

    @pytest.fixture(autouse=True)
    def api_set_up(self, check_status_set_up, api_client_set_up, api_baseops_set_up):
        """Sets up status/api_client objects"""

    def test_crud_vacancies(self):
        """Runs full CRUD (POST - GET - PUT - DELETE) test"""
        # GET the existing vacancies (we can create a precondition fixture for ex.)
        # Now I just use the previously created
        response_get = self.api_client._make_request(
            'get', f'/accounts/{settings.ACCOUNT_ID}/vacancies')
        result_get = self.api_baseops.is_response_valid(
            response_get, settings.RESPONSE_CRUD_GET)
        self.status.set_result("test_crud_vacancies", result_get)
        # CREATE a new vacancy
        response_post = self.api_client._make_request(
            'post', f'/accounts/{settings.ACCOUNT_ID}/vacancies',
            json = settings.PAYLOAD_CRUD_POST)
        vacancy_id = response_post["id"]
        result_post = self.api_baseops.is_response_valid(
            response_post, settings.RESPONSE_CRUD_POST, new_keys = settings.RESPONSE_NEW_KEYS_POST)
        self.status.set_result("test_crud_vacancies", result_post)
        # UPDATE a new vacancy
        response_put = self.api_client._make_request(
            'put', f'/accounts/{settings.ACCOUNT_ID}/vacancies/{vacancy_id}',
            json = settings.PAYLOAD_CRUD_PUT)
        result_put = self.api_baseops.is_response_valid(
            response_put, settings.RESPONSE_CRUD_PUT, new_keys = settings.RESPONSE_NEW_KEYS_PUT)
        self.status.set_result("test_crud_vacancies", result_put)
        # DELETE a new vacancy
        response_delete = self.api_client._make_request(
            'delete', f'/accounts/{settings.ACCOUNT_ID}/vacancies/{vacancy_id}')
        result_delete = self.api_baseops.is_response_valid(response_delete)
        self.status.mark_final("test_crud_vacancies", result_delete)
