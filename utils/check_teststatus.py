"""Checks test results"""
import logging


class CheckStatus:
    """Checking tests results"""
    logger = logging.getLogger(__name__)

    def __init__(self):
        self.result_list = []

    def set_result(self, test_name, result):
        """Intermediate result"""
        if result:
            self.result_list.append("PASS")
            self.logger.info('VERIFICATION SUCCESSFUL: %s', test_name)
        else:
            self.result_list.append("FAIL")
            self.logger.error('VERIFICATION FAILED: %s', test_name)

    def mark_final(self, test_name, result):
        """Final result"""
        self.set_result(test_name, result)
        assert 'FAIL' not in self.result_list
        self.result_list.clear()
