"""
Represents actions on the Iframe page
"""
import logging
import settings
from base.baseops import BaseOps


class IframePage(BaseOps):
    """Actions on the Form page"""
    logger = logging.getLogger(__name__)

    def __init__(self, driver):
        super().__init__(driver)
        self.email = self.create_mail()

    def fill_form(self, **kwargs):
        """Filling the Iframe form"""
        self.wait_until_element(settings.MAIN_IFRAME, settings.FRAME_SWITCH)
        self.sending_keys(kwargs['fname'], settings.F_NAME_IFRAME)
        self.sending_keys(kwargs['lname'], settings.L_NAME_EMAIL_IFRAME)
        self.wait_until_element(settings.INNER_IFRAME, settings.FRAME_SWITCH)
        self.sending_keys(self.email, settings.L_NAME_EMAIL_IFRAME)
        self.driver.switch_to.default_content()
        self.wait_until_element(settings.MAIN_IFRAME, settings.FRAME_SWITCH)
 