"""
Represents actions on the Form page
"""
import logging
from selenium.webdriver.support.ui import Select
import settings
from base.baseops import BaseOps


class FormPage(BaseOps):
    """Actions on the Form page"""
    logger = logging.getLogger(__name__)

    def __init__(self, driver):
        super().__init__(driver)
        self.email = self.create_mail()

    def fill_form(self, **kwargs):
        """Filling the form page"""
        parameters = (
            (kwargs['fname'], settings.FIRST_NAME),
            (kwargs['lname'], settings.LAST_NAME),
            (kwargs['ccode'], settings.COUNTRY_CODE),
            (kwargs['pnumber'], settings.PHONE_NUMBER),
            (kwargs['adline1'], settings.ADDRESS_LINE_1),
            (kwargs['adline2'], settings.ADDRESS_LINE_2),
            (kwargs['state'], settings.STATE),
            (kwargs['pcode'], settings.POSTAL_CODE),
            (kwargs['ctry'], settings.COUNTRY),
            (kwargs['birth'], settings.DATE_OF_BIRTH),
            (self.email, settings.EMAIL),
        )
        parameters_click = (
            settings.GENDER, settings.TERMS_CHECKBOX,
            settings.SUBMIT_BUTTON,
        )
        for data, locator in parameters:
            if data in(kwargs['ccode'], kwargs['ctry']):
                select = Select(self.find_an_element(locator))
                select.select_by_value(data)
            else:
                self.clear_field(locator)
                self.sending_keys(data, locator)
        for button in parameters_click:
            self.driver.execute_script("window.scrollBy(0, 200); ")
            self.wait_until_element(button, settings.EL_CLICKABLE, click_condition=True)
