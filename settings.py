"""
Global variables and configs
"""
from os.path import join, dirname
from dotenv import dotenv_values

dotenv_path = join(dirname(__file__), '.env')
values = dotenv_values(dotenv_path)
config = values


# Logger config
LOGGING_CONFIG = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': '%(asctime)s - %(name)s - %(levelname)s: %(message)s'
        },
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'formatter': 'standard',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'automation.log',
            'mode': 'w'
        },
        'console': {
            'level': 'ERROR',
            'formatter': 'standard',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout',
        },
    },
    'loggers': {
        '': {  #  root logger
            'handlers': ['file', 'console'],
            'level': 'DEBUG'
        }
    }
}

# General navigation
MAIN_PAGE = "test"
FORM_PAGE = "forms"
IFRAME_PAGE = "frame"

# General vars
TESTNAME = "Test"
TESTPASSWORD = "Test123"
NEW_PASSWORD = "NewTest123"

# Mapping keys locator types
CSS = "CSS"
CLASS = "CLASS"
ID = "ID"
LINK = "LINK"
NAME = "NAME"
XPATH = "XPATH"

# Mapping wait conditions
EL_CLICKABLE = "element_to_be_clickable"
FRAME_SWITCH = "frame_to_be_available_and_switch_to_it"
PR_EL_LOCATED = "presence_of_element_located"
TITLE_IS = "title_is"
URL_CONTAINS = "url_contains"
URL_TO_BE = "url_to_be"

# Waits
WAIT_SECOND = 1
WAIT_LITTLE = 3
WAIT_AVERAGE = 20
WAIT_LONG = 60

# FORM PAGE VARS
FIRST_NAME = '//*[@id="firstname"]'
LAST_NAME = '//*[@id="lasttname"]'
EMAIL = '//*[@id="email"]'
COUNTRY_CODE = '//label[@id="countrycode"]//parent::div//div[@class="select"]/select'
PHONE_NUMBER = '//*[@id="Phno"]'
ADDRESS_LINE_1 = '//*[@id="Addl1"]'
ADDRESS_LINE_2 = '//*[@id="Addl2"]'
STATE = '//*[@id="state"]'
POSTAL_CODE = '//*[@id="postalcode"]'
COUNTRY = '//label[@id="country"]//parent::div//div[@class="select"]/select'
DATE_OF_BIRTH = '//*[@id="Date"]'
GENDER = '//label[@class="radio"][2]/input'
TERMS_CHECKBOX = '//label[@class="checkbox"]/input'
SUBMIT_BUTTON = '//input[@class="button is-primary"]'

CHECK_EMAIL = '//input[@value="hello@"]'

# IFRAME PAGE VARS
MAIN_IFRAME = '//*[@id="firstFr"]'
INNER_IFRAME = '//iframe[@class="has-background-white"]'
F_NAME_IFRAME = '//input[@placeholder="Enter name"]'
L_NAME_EMAIL_IFRAME = '//input[@placeholder="Enter email"]'
FILLED_IFRAME = '//p[text()="You have entered fname lname"]'

# ANY TEST VARS
ACCOUNT_ID = 41
REFRESH_ENDPOINT = 'token/refresh'

RESPONSE_CRUD_GET = {
  "page": 1,
  "count": 30,
  "total_pages": 1,
  "items": [
    {
      "account_division": None,
      "account_region": None,
      "position": "Backend-разработчик",
      "company": None,
      "money": None,
      "priority": 0,
      "hidden": False,
      "state": "OPEN",
      "id": 211,
      "created": "2023-02-03T07:37:43+03:00",
      "additional_fields_list": [],
      "multiple": False,
      "parent": None,
      "account_vacancy_status_group": None
    },
    {
      "account_division": None,
      "account_region": None,
      "position": "Frontend-разработчик",
      "company": None,
      "money": None,
      "priority": 0,
      "hidden": False,
      "state": "OPEN",
      "id": 209,
      "created": "2023-02-03T07:37:43+03:00",
      "additional_fields_list": [],
      "multiple": False,
      "parent": None,
      "account_vacancy_status_group": None
    },
    {
      "account_division": None,
      "account_region": None,
      "position": "Бариста",
      "company": None,
      "money": None,
      "priority": 0,
      "hidden": False,
      "state": "OPEN",
      "id": 212,
      "created": "2023-02-03T07:37:43+03:00",
      "additional_fields_list": [],
      "multiple": False,
      "parent": None,
      "account_vacancy_status_group": None
    },
    {
      "account_division": None,
      "account_region": None,
      "position": "Менеджер по продажам",
      "company": None,
      "money": None,
      "priority": 0,
      "hidden": False,
      "state": "OPEN",
      "id": 210,
      "created": "2023-02-03T07:37:43+03:00",
      "additional_fields_list": [],
      "multiple": False,
      "parent": None,
      "account_vacancy_status_group": None
    }
  ]
}

PAYLOAD_CRUD_POST = {
  "position": "Developer",
  "company": "Google",
  "money": "$10000",
  "priority": 0,
  "hidden": False,
  "state": "OPEN",
  "coworkers": [
    38
  ],
  "body": "<p>Test body</p>",
  "requirements": "<p>Test requirements</p>",
  "conditions": "<p>Test conditions</p>",
  "fill_quotas": [
    {
      "deadline": "2030-01-01",
      "applicants_to_hire": 1
    }
  ]
}

RESPONSE_CRUD_POST = {
  "account_division": None,
  "account_region": None,
  "position": "Developer",
  "company": "Google",
  "money": "$10000",
  "priority": 0,
  "hidden": False,
  "state": "OPEN",
  "coworkers": None,
  "body": "<p>Test body</p>",
  "requirements": "<p>Test requirements</p>",
  "conditions": "<p>Test conditions</p>",
  "files": None,
  "account_vacancy_status_group": None,
  "parent": None,
  "source": None,
  "multiple": False,
  "vacancy_request": None
}

RESPONSE_NEW_KEYS_POST = {
    "id": int,
    "created": str
    }

PAYLOAD_CRUD_PUT = {
  "position": "Superstar Senior Developer :)",
  "company": "Google",
  "money": "$10000000",
  "priority": 0,
  "hidden": False,
  "state": "OPEN",
  "coworkers": [
    38
  ],
  "body": "<p>Test body</p>",
  "requirements": "<p>Test requirements</p>",
  "conditions": "<p>Test conditions</p>"
}

RESPONSE_CRUD_PUT = {
  "account_division": None,
  "account_region": None,
  "position": "Superstar Senior Developer :)",
  "company": "Google",
  "money": "$10000000",
  "priority": 0,
  "hidden": False,
  "state": "OPEN",
  "additional_fields_list": [],
  "multiple": False,
  "parent": None,
  "account_vacancy_status_group": None,
  "body": "<p>Test body</p>",
  "requirements": "<p>Test requirements</p>",
  "conditions": "<p>Test conditions</p>",
  "files": [],
  "source": None,
  "blocks": []
}

RESPONSE_NEW_KEYS_PUT = {
    "id": int,
    "created": str,
    "updated": str
}
